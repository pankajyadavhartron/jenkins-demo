(function() {
    'use strict';

    angular
        .module('jenkinsApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
